import React from 'react'

const Footer = () => {
    return (
        <>
            <section className="w-full bg-white">
                <div className="px-8 py-12 mx-auto max-w-7xl">
                    <div className="flex flex-col items-start justify-between pt-10 mt-10 border-t border-gray-100 md:flex-row md:items-center">
                        <p className="mb-6 text-sm text-left text-gray-600 md:mb-0">© 2024 PT Markplus Indonesia.</p>
                        <div className="flex items-start justify-start space-x-6 md:items-center md:justify-center">
                            <a href="#_" className="text-sm text-gray-600 transition hover:text-primary">Terms</a>
                            <a href="#_" className="text-sm text-gray-600 transition hover:text-primary">Privacy</a>
                        </div>
                    </div>
                </div>
            </section>
        </>
    )
}

export default Footer