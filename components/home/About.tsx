import React from 'react'

const About = () => {
  return (
    <div className="sm:w-full mx-auto lg:mt-28 mt-16">
      <div className="max-w-3xl mx-auto lg:my-24 lg:mb-18 text-center lg:px-2 px-6">
        <div className="lg:text-4xl text-xl font-bold text-[#1B1072] lg:mb-10 mb-4 lg:tracking-[.40em] tracking-[.20em] uppercase">about</div>
        <div className="items-center lg:text-xl text-base  text-black p-1" >
        Marketeers Hangout merupakan tempat kumpulnya orang-orang marketing yang kreatif. Di sini, para marketing antusias bisa berbagi inspirasi, solusi kreatif, dan jejaring baru. 
Para marketing enthusiast dapat merasakan pengalaman yang imersif, seperti paparan pembicara yang terbalut rapih dengan pertunjukan teater hingga permainan berjejaring yang melibatkan peserta, pembicara, dan artis teater. 
Marketeers Hangout mengusung konsep konferensi marketing bergaya teatrikal, dengan konsep ini, Anda bisa menyimak paparan pembicara secara lebih menyenangkan dan menghibur seperti saat menikmati pertunjukan teater. 
Anda siap menjadi orang marketing yang kreatif? Mari kita hangout bersama di sini.
        </div>
        <div className="mt-10 mb-10">
          <iframe
            className='rounded-2xl w-full'
            width="672"
            height="378"
            src="https://www.youtube.com/embed/IxBPVKUB3I0"
            title="Marketeers Hangout 2023 - Highlight" frameBorder="0"
            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerPolicy="strict-origin-when-cross-origin"
            allowFullScreen>
          </iframe>
        </div>
      </div>

    </div>
  )
}

export default About