'use client'
import { NAV_LINKS } from '@/constans'
import Image from 'next/image'
import Link from 'next/link'
import React, { useEffect, useRef } from 'react'
import { AiOutlineMenu } from 'react-icons/ai'
import 'aos/dist/aos.css'
import Aos from 'aos'

const Navbar = () => {
    const [open, setOpen] = React.useState(false);
    const [isScrolling, setIsScrolling] = React.useState(false)

    React.useEffect(() => {
        window.addEventListener(
            "resize",
            () => window.innerWidth >= 960 && setOpen(false)
        );
    }, []);

    React.useEffect(() => {
        function handleScroll() {
            if (window.scrollY > 0) {
                setIsScrolling(true);
            } else {
                setIsScrolling(false);
            }
        }

        window.addEventListener("scroll", handleScroll);

        return () => window.removeEventListener("scroll", handleScroll);
    }, [])
    useEffect(() => {
        Aos.init({
          duration: 2000,
          easing: 'ease-out-cubic',
        })
      }, [])
    const registerRef = useRef<HTMLDivElement>(null)
    function handleRegister() {
        if (registerRef && registerRef.current) {
          registerRef.current.scrollIntoView({ behavior: 'smooth' })
        }
      }
    return (
        <nav className={isScrolling ? 'fixed w-full bg-white lg:h-24 h-16 shadow-lg top-0 left-0 z-10 ' : 'fixed w-full bg-transparent lg:h-24 h-16 shadow-lg top-0 left-0 z-10 '}>
            <div className="flex justify-between items-center h-full w-full lg:px-20 px-4">
                <Link href="/" className="lg:h-3/4 h-auto lg:mt-4">
                    <Image
                        src="/../../assets/images/mho_2024logo.png"
                        width={74}
                        height={29}
                        alt="Marketeers Hangout"
                        className="lg:w-full h-auto"
                    />
                </Link>
                <ul className='hidden gap-10 lg:flex ml-4 mr-4'>
                    {NAV_LINKS.map((dt, i) => (
                        <Link href={dt.href} key={i} className={isScrolling ? 'text-sm items-center cursor-pointer transition-all font-bold uppercase' : 'text-sm items-center cursor-pointer transition-all text-white font-bold uppercase'} >{dt.label}</Link>
                    ))}
                </ul>
                <div className='hidden lg:flex'>
                    {/* <button type='button' className='text-sm capitalize items-center cursor-pointer transition-all hover:font-bold'>Buy Tickets</button> */}
                </div>
                <div className="sm:hidden cursor-pointer">
                    <AiOutlineMenu className={isScrolling ? 'text-black': 'text-white font-bold'} />
                </div>
            </div>
        </nav>
    )
}

export default Navbar