'use client'

import About from '@/components/home/About'
import Agenda from '@/components/home/Agenda'
import Footer from '@/components/home/Footer'
import Hero from '@/components/home/Hero'
import Parallax from '@/components/home/Parallax'
import Register from '@/components/home/Register'
import Speakers from '@/components/home/Speakers'
import Sponsorship from '@/components/home/Sponsorship'
import Vanue from '@/components/home/Vanue'
import Whyattend from '@/components/home/Whyattend'
import Whyshouldattend from '@/components/home/Whyshouldattend'
import Images from '@/components/image/Images'
import { NAV_LINKS } from '@/constans'
import Aos from 'aos'
import Image from 'next/image'
import Link from 'next/link'
import React, { useEffect, useRef, useState } from 'react'
import { AiFillCaretDown, AiOutlineClose, AiOutlineMenu } from 'react-icons/ai'

export default function Home() {
  const [open, setOpen] = React.useState(false);
  const [isScrolling, setIsScrolling] = React.useState(false)
  const [menuOpen, setMenuOpen] = useState(false)

  React.useEffect(() => {
    window.addEventListener(
      "resize",
      () => window.innerWidth >= 960 && setOpen(false)
    );
  }, []);

  React.useEffect(() => {
    function handleScroll() {
      if (window.scrollY > 0) {
        setIsScrolling(true);
      } else {
        setIsScrolling(false);
      }
    }

    window.addEventListener("scroll", handleScroll);

    return () => window.removeEventListener("scroll", handleScroll);
  }, [])
  useEffect(() => {
    Aos.init({
      duration: 2000,
      easing: 'ease-out-cubic',
    })
  }, [])
  function handleNavigation(anchor: string) {
    const element = document.getElementById(anchor);
    if (element) {
      element.scrollIntoView({ behavior: 'smooth' });
    }
  }

  const handleNav = () => {
    setMenuOpen(!menuOpen)
  }
  const [position, setPosition] = useState({ x: 0, y: 0 })

  const handleMouseMove = (e: any) => {
    setPosition({
      x: e.clientX / window.innerWidth,
      y: e.clientY / window.innerHeight,
    })
  }
  return (
    <>
      <title>Marketing Hangout | Marketing vs Everybody</title>
      <meta name="googlebot-news" content="index,follow" />
      <meta name="googlebot" content="index,follow" />
      <meta name="robots" content="index, follow" />
      <meta name="keywords" content="Indonesia marketing festival" />
      <meta name="news_keywords" content="marketeers hangout" />
      <meta property="og:type" content="article" />
      <meta property="og:site_name" content="www.indonesiamarketingfestival.com" />
      <meta property="og:creator" content="markplusinc" />
      <meta property="og:image" content="https://mho-navy.vercel.app/assets/images/mho_2024cover.jpg" />
      <meta property="og:image:width" content="1200" />
      <meta property="og:image:height" content="630" />
      <meta property="og:image:type" content="image/jpg" />
      <meta property="og:locale" content="id_ID" />
      <meta property="og:title" content="Marketeers Hangout" />
      <meta property="og:description" content="Marketing vs Everybody" />
      <meta property="og:url" content="https://www.marketeershangout.com/" />
      <meta name="twitter:card" content="summary_large_image" />
      <meta name="twitter:site" content="@the_marketeers" />
      <meta name="twitter:creator" content="@the_marketeers" />
      <meta name="twitter:title" content="Marketing Hangout | Marketing vs Everybody" />
      <meta name="twitter:description" content="MarkPlus Inc. operates through four distinct business divisions: MarkPlus Consulting, MarkPlus Insight, MarkPlus Tourism, and MarkPlus Islamic. MarkPlus Consulting serves as the premier consulting arm of our organization, dedicated to delivering innovative solutions and strategic guidance across various marketing challenges. MarkPlus Insight specializes in comprehensive marketing and social research, delivering actionable insights and reliable data to drive informed decision-making. MarkPlus Tourism and MarkPlus Islamic divisions were established to cater to the specific needs of the rapidly expanding tourism and Islamic economy sectors, offering tailored research and consultancy services to meet the diverse demands of these industries" />
      <meta name="twitter:image" content="/assets/images/mho_2024cover.jpg" />
      <meta name="content_tags" content="Marketing Hangout | Marketing vs Everybody" />
      <script
        type="application/ld+json"
        dangerouslySetInnerHTML={{ __html: JSON.stringify("Marketing Hangout | Marketing vs Everybody") }}
      />
      {/* NAVIGASI */}
      <nav className='fixed w-full bg-white lg:h-14 h-16 shadow-lg top-0 left-0 z-10 '>
        <div className="flex lg:justify-between items-center h-full w-full lg:px-20 px-4">
          <div onClick={handleNav} className="sm:hidden cursor-pointer">
            <AiOutlineMenu className='text-black' />
          </div>
          <button onClick={() => handleNavigation('hero')} className="lg:h-3/4 h-auto lg:mt-0 ml-4">
            <Image
              src="/assets/images/imf/logo.png"
              width={45}
              height={25}
              alt="Marketeers Hangout"
              className="lg:w-full lg:h-auto"
            />
          </button>
          <ul className='hidden gap-10 lg:flex ml-4 mr-4'>
            {NAV_LINKS.map((dt, i) => (
              <li><button onClick={() => handleNavigation(dt.href)} key={i} className='text-sm items-center cursor-pointer transition-all text-black font-bold uppercase btn-hover' >{dt.label}</button></li>
            ))}
          </ul>

          <div className={
            menuOpen
              ? "fixed left-0 top-16 w-3/4 sm:hidden h-screen overflow-y-auto bg-black p-10 ease-in duration-700"
              : "fixed left-[-100%] w-full bg-black top-16 p-10  h-screen ease-in duration-700"
          }>
            <div className="flex w-full">
              <ul className='gap-10'>
                {NAV_LINKS.map((dt, i) => (
                  <li>
                    <button onClick={() => { handleNavigation(dt.href); handleNav(); }} key={i} className='text-white text-[12px] cursor-pointer transition-all  uppercase p-2'>{dt.label}</button>
                  </li>
                ))}
              </ul>
            </div>
          </div>
        </div>
      </nav>
      {/* END NAVIGASI */}
      {/* HERO SECTION */}
      <div id='hero'></div>
      <div className="relative h-screen w-full">
      <div className='w-full relative lg:bg-landscape bg-mobile bg-cover h-screen overflow-hidden'></div>
      <div className="absolute inset-0 flex items-center justify-center">
        <div className="relative text-center lg:w-3/6 p-2 flex flex-col items-center">
          <Image
            src={'/assets/images/imf/IMF24_Logo_IMF.png'}
            alt={"Picture of the author"}
            width={500}
            height={300}
            className='w-3/6 h-auto mb-7'
            sizes="100vw"
            placeholder="blur"
            blurDataURL={`/assets/images/bpou.jpg`}
          />
          <Image
            src={'/assets/images/imf/IMF24_Logo_Pilot_Marketing.png'}
            alt={"Picture of the author"}
            width={500}
            height={300}
            className='w-full h-auto mt-3'
            sizes="100vw"
            placeholder="blur"
            blurDataURL={`/assets/images/bpou.jpg`}
          />
          <div className="relative p-8 lg:my-auto lg:mx-auto mx-2 my-2 mt-4  grid place-items-center text-center ">
            <span className="lg:max-w-6xl lg:text-base text-lg font-bold lg:tracking-[.45em] uppercase text-white">Agustus 2024</span>
            <span className="mb-2 lg:text-xl text-[12px] w-full text-white uppercase">
              JOGJA * PEKANBARU * PALEMBANG * MANADO * DENPASAR * BANDUNG * SURABAYA
            </span>
            <Image
              src={'/assets/images/imf/IMF24_Badge_Day_1&2.png'}
              alt={"Picture of the author"}
              width={500}
              height={300}
              className='w-full h-auto mt-3'
              sizes="100vw"
              placeholder="blur"
              blurDataURL={`/assets/images/bpou.jpg`}
            />
          </div>
        </div>
      </div>
    </div>
      {/* END HERO SECTION */}
      {/* ABOUT */}
      <div id="whoshouldattend"></div>
      <Whyshouldattend />
      {/* END ABOUT */}
      {/* VENUE */}
      <div id="agenda"></div>
      <Agenda />
      {/* END VENUE */}
      {/* PSEAKER */}
      <div id="speakers"></div>
      <Speakers />
      {/* END SPEAKER */}
      {/* REGISTER */}
      <div id="register"></div>
      <Register />
      {/* END REGISTER */}
      
      <Footer />
    </>
  )
}
