import type { Metadata } from 'next'
import './globals.css'
import Navbar from '@/components/Navbar'
import "slick-carousel/slick/slick.css"
import "slick-carousel/slick/slick-theme.css"

export const metadata: Metadata = {
  title: 'Indonesia Marketing Festival ',
  description: 'indonesiamarketingfestival.com',
}

export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <html lang="en">
      <body>
        <main className='relative overflow-hidden'>
          {children}
        </main>        
      </body>
    </html>
  )
}
