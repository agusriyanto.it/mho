import React, { FC, useState } from 'react'
import Image from 'next/image'

const Images = () => {
    const [position, setPosition] = useState({ x: 0, y: 0 })
    const handleMouseMove = (e: any) => {
        setPosition({
            x: e.clientX / window.innerWidth,
            y: e.clientY / window.innerHeight,
        })
    }

    return (
        <Image
            src={'/assets/images/marketing/m.png'}
            alt={`Picture`}
            layout="fill"
            objectFit="contain"
            className='layer'
            style={{
                transform: `translate(${position.x * 40}px, ${position.y * 20}px)`
            }}
            quality={100}
        />
    )
}

export default Images
