import Image from 'next/image'
import Link from 'next/link'
import React from 'react'

const Agenda = () => {
  return (
    <div className="w-full bg-black mx-auto mt-20 p-4">
      <div className="w-2/4 mx-auto lg:my-24 lg:mb-18 mt-8">
        <div className="lg:text-xl text-base font-bold text-center text-[#1B1072]  lg:mb-10 font-body uppercase">
          <h4 className='lg:text-5xl text-white lg:tracking-[.40em] tracking-[.20em] font-bold uppercase'>Agenda</h4>
        </div>
        <div className="lg:flex text-center justify-center gap-4">
          <div className="w-full gap-2 items-center justify-between">
            <div className='lg:mt-10'>
              <h2 className='text-white mb-4 lg:text-2xl text-base'>DAY 1</h2>
              <Image
                src="/assets/images/imf/IMF24_Schedule_Day_1.png"
                alt="Marketeers Hangout"
                width={200}
                height={112.5}
                quality={90}
                className='w-full h-auto'
                sizes='(max-width: 768px) 100vw, (max-width: 1200px) 50vw, 33vw'
              />
            </div>
            <div className='lg:mt-10'>
              <h2 className='text-white mb-4 lg:text-2xl text-base'>DAY 2</h2>
              <Image
                src="/assets/images/imf/IMF24_Schedule_Day_2.png"
                alt="Marketeers Hangout"
                width={200}
                height={112.5}
                quality={90}
                className='w-full h-auto'
                sizes='(max-width: 768px) 100vw, (max-width: 1200px) 50vw, 33vw'
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Agenda