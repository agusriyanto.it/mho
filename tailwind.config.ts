/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './pages/**/*.{js,ts,jsx,tsx,mdx}',
    './components/**/*.{js,ts,jsx,tsx,mdx}',
    './app/**/*.{js,ts,jsx,tsx,mdx}',
  ],
  theme: {
    extend: {
      backgroundImage: {
        'parallax' : 'url("/assets/images/saleh-unsplash.jpg")',
        'hero-parallax' : 'linear-gradient(to bottom,rgba(220, 99, 71, 0.2),rgba(0, 0, 0, 1)),url("/assets/images/mho_2024cover.jpg")',
        'landscape' : 'url("/assets/images/imf/Logo_Landscape.png")',
        'mobile' : 'url("/assets/images/imf/logo_Portrait.png")',
        'about' : 'url("/assets/images/bg-o.jpg")',
      }
    },
  },
  plugins: [],
};