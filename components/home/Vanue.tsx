'use client'

import Image from 'next/image'
import React from 'react'
// import Slider from 'react-slick'

const Vanue = () => {
  var settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1
  };
  return (
    <div className="sm:w-full mx-auto mt-20">
      <div className="max-w-6xl mx-auto lg:my-24 lg:mb-18  lg:px-2 mt-8 px-6">
        <div className="lg:text-4xl text-base font-bold text-center text-[#1B1072] mb-10  lg:tracking-[.40em] uppercase" data-aos="fade-down" data-aos-delay="200">
          <h4>vanue</h4>
        </div>
        <div className="grid text-center justify-center">
          <h4 className="text-2xl  uppercase font-bold text-black tracking-[.40em]">Ciputra</h4>
          <h4 className="text-2xl  uppercase font-bold text-black tracking-[.40em]">Artpreneur</h4>
          <h4 className="text-xl  mt-4 tracking-[.20em]">Ciputra World, Retail Podium, Jl. Prof. DR. Satrio No.1</h4>
          <h4 className="text-xl  tracking-[.20em]">Jakarta Selatan</h4>
          <div className="grid  font-bold text-black text-2xl items-center">
          </div>
        </div>
        <div className='w-full lg:grid lg:grid-cols-3 gap-4 h-auto mt-10'>
          {/* <Slider {...settings}> */}
          <div className="relative w-full hover:brightness-50 lg:h-auto mb-4" >
            <div className='w-full relative lg:h-auto'>
              <Image
                src={'/assets/images/Front.jpg'}
                alt={"Picture of the author"}
                width={200}
                height={112.5}
                quality={100}
                priority
                className='w-full h-auto'
                sizes="100vw"
                style={{
                  objectFit: 'cover',
                }}
              />
            </div>
          </div>
          {/* <div className='absolute max-w-7xl p-4'>
            <div className="grid lg:w-full h-auto">
              <div className='mt-4 text-2xl lg:w-full lg:mb-32 mb-80 md:text-4xl font-bold font-banner uppercase'>
                <h4 className='flex text-white text-base uppercase font-bold font-title'>lorem</h4>
                <p className='text-white text-sm'>Lorem, ipsum dolor.</p>
              </div>
            </div>
          </div> */}

          <div className="relative w-full lg:h-auto hover:brightness-50 mb-4" >
            <div className='w-full relative lg:h-auto'>
              <Image
                src={'/assets/images/Front.jpg'}
                alt={"Picture of the author"}
                width={200}
                height={112.5}
                quality={100}
                priority
                className='w-full h-auto'
                sizes="100vw"
                style={{
                  objectFit: 'cover',
                }}
              />
            </div>
          </div>
          {/* <div className='absolute max-w-7xl p-4 hover'>
            <div className="grid lg:w-full h-auto">
              <div className='mt-4 text-2xl lg:w-full lg:mb-32 mb-80 md:text-4xl font-bold font-banner uppercase'>
                <h4 className='flex text-white text-base uppercase font-bold font-title'>lorem5</h4>
                <p className='text-white text-sm'>Lorem, ipsum dolor.</p>
              </div>
            </div>
          </div> */}

          <div className="relative w-full lg:h-auto hover:brightness-50 mb-4" >
            <div className='w-full relative lg:h-auto'>
              <Image
                src={'/assets/images/Front.jpg'}
                alt={"Picture of the author"}
                width={200}
                height={112.5}
                quality={100}
                priority
                className='w-full h-auto'
                sizes="100vw"
                style={{
                  objectFit: 'cover',
                }}
              />
            </div>
          </div>
          {/* <div className='absolute max-w-7xl p-4'>
            <div className="grid lg:w-full h-auto">
              <div className='mt-4 text-2xl lg:w-full lg:mb-32 mb-80 md:text-4xl font-bold font-banner uppercase'>
                <h4 className='flex text-white text-base uppercase font-bold font-title'>lorem5</h4>
                <p className='text-white text-sm'>Lorem, ipsum dolor.</p>
              </div>
            </div>
          </div> */}
          <div className="relative w-full lg:h-auto hover:brightness-50 mb-4" >
            <div className='w-full relative lg:h-auto'>
              <Image
                src={'/assets/images/Front.jpg'}
                alt={"Picture of the author"}
                width={200}
                height={112.5}
                quality={100}
                priority
                className='w-full h-auto'
                sizes="100vw"
                style={{
                  objectFit: 'cover',
                }}
              />
            </div>
          </div>
          {/* <div className='absolute max-w-7xl p-4'>
            <div className="grid lg:w-full h-auto">
              <div className='mt-4 text-2xl lg:w-full lg:mb-32 mb-80 md:text-4xl font-bold font-banner uppercase'>
                <h4 className='flex text-white text-base uppercase font-bold font-title'>lorem5</h4>
                <p className='text-white text-sm'>Lorem, ipsum dolor.</p>
              </div>
            </div>
          </div> */}
          <div className="relative w-full lg:h-auto hover:brightness-50 mb-4" >
            <div className='w-full relative lg:h-auto'>
              <Image
                src={'/assets/images/Front.jpg'}
                alt={"Picture of the author"}
                width={200}
                height={112.5}
                quality={100}
                priority
                className='w-full h-auto'
                sizes="100vw"
                style={{
                  objectFit: 'cover',
                }}
              />
            </div>
          </div>
          {/* <div className='absolute max-w-7xl p-4'>
            <div className="grid lg:w-full h-auto">
              <div className='mt-4 text-2xl lg:w-full lg:mb-32 mb-80 md:text-4xl font-bold font-banner uppercase'>
                <h4 className='flex text-white text-base uppercase font-bold font-title'>lorem5</h4>
                <p className='text-white text-sm'>Lorem, ipsum dolor.</p>
              </div>
            </div>
          </div> */}
          <div className="relative w-full lg:h-auto hover:brightness-50 mb-4" >
            <div className='w-full relative lg:h-auto'>
              <Image
                src={'/assets/images/Front.jpg'}
                alt={"Picture of the author"}
                width={200}
                height={112.5}
                quality={100}
                priority
                className='w-full h-auto'
                sizes="100vw"
                style={{
                  objectFit: 'cover',
                }}
              />
            </div>
          </div>
          {/* <div className='absolute max-w-7xl p-4'>
            <div className="grid lg:w-full h-auto">
              <div className='mt-4 text-2xl lg:w-full lg:mb-32 mb-80 md:text-4xl font-bold font-banner uppercase'>
                <h4 className='flex text-white text-base uppercase font-bold font-title'>lorem5</h4>
                <p className='text-white text-sm'>Lorem, ipsum dolor.</p>
              </div>
            </div>
          </div> */}
          {/* </Slider>  */}
        </div>
      </div>
    </div>
  )
}

export default Vanue 