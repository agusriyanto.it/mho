'use client'

import Image from 'next/image'
import React, { useRef } from 'react'
import ButtonRegister from '../ButtonRegister'
import Link from 'next/link'
import { AiFillCaretDown } from 'react-icons/ai'
import Register from './Register'
import About from './About'

const Hero = () => {
  const registerRef = useRef<HTMLDivElement>(null)
  function handleRegister() {
    if (registerRef && registerRef.current) {
      registerRef.current.scrollIntoView({ behavior: 'smooth' })
    }
  }

  return (
    <>
      <div className="relative lg:top-0 -top-16 min-h-screen w-full">
        <Image
          src={'/../../assets/images/mho_2024cover.jpg'}
          alt={"Picture of the author"}
          fill
          className='w-full bg-cover bg-no-repeat'
          objectFit="cover"
          sizes="100vw"
          placeholder="blur"
          blurDataURL={`/../../assets/images/bpou.jpg`}
        />
        <div className="absolute inset-0 h-full w-full bg-blue-950/80" />
        <div className="grid min-h-screen px-8">
          <div className="relative p-8 lg:mt-52 lg:my-auto lg:mx-auto mx-2 my-36 mt-60  grid place-items-center text-center ">
            {/* <span className="mb-0 lg:text-xl text-base uppercase tracking-[.40em]  text-white">marketeers hangout</span> */}
            <span className="lg:max-w-6xl lg:text-7xl font-extrabold uppercase text-white">Marketing vs Everybody</span>
            <span className="mb-0 lg:text-base text-[12px] w-full md:max-w-full lg:max-w-2xl text-white tracking-[.40em] uppercase">
              Tempat Ngumpul Orang Marketing
            </span>
            
            <span className="mb-12 lg:text-lg text-[14px] uppercase tracking-[.45em]  text-white">5 September 2024</span>
            <AiFillCaretDown  className='text-white w-full lg:text-6xl text-2xl animate-bounce'/>
            <div className="flex items-center gap-4">
              <Link href='#register' className="buttonsm uppercase hover:scale-110">
                Gabung Sekarang
              </Link>
            </div>
          </div>
        </div>
      </div>
      {/* REGISTER SECTION */}
    </>
  )
}

export default Hero