'use client'

import React, { useEffect, useState } from 'react'
import { AiFillCaretDown } from 'react-icons/ai'

const ButtonRegister = () => {
    const [showScroll, setShowScroll] = useState(false);

    useEffect(() => {
        const handleScroll = () => {
            if (window.scrollY > 300) {
                setShowScroll(true)
            } else {
                setShowScroll(false)
            }
        }
        window.addEventListener('scroll', handleScroll)
        return () => window.removeEventListener('scroll', handleScroll)
    }, [])

    const scrollToBottom = () => {
        window.scrollTo({ top: window.scrollY + window.innerHeight, behavior: 'smooth' })
    }

    return (
        <div onClick={scrollToBottom} className=' text-6xl z-50' style={{ display: showScroll ? 'flex' : 'block' }}>
            <button >
                    <>
                        <AiFillCaretDown  className='text-white lg:text-7xl text-xl z-20 animate-bounce'/>
                        <h5 className='text-4xl font-body uppercase text-black font-bold'>Join Sekarang</h5>
                    </>
                
            </button>
            
        </div>
    )
}


export default ButtonRegister