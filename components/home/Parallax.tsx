'use client'
import Image from 'next/image'
import React, { useState } from 'react'

const Parallax = () => {
    const [position, setPosition] = useState({ x: 0, y: 0 });

    const handleMouseMove = (e: any) => {
        setPosition({
            x: e.clientX / window.innerWidth,
            y: e.clientY / window.innerHeight,
        });
    };

    // const handleMouseLeave = () => {
    //     setPosition({ x: 0, y: 0 });
    // };

    return (
        <div className="gap-4 lg:mt-24 mt-16" >
            <div className="relative w-full" onMouseMove={handleMouseMove} >
                <div className='w-full relative bg-parallax-notitle bg-cover h-screen overflow-hidden'>
                    <>
                        <Image
                            src={'/assets/images/parallax/1.png'}
                            alt={`Picture `}
                            layout="fill"
                            objectFit="contain"
                            className='layer img-parallax'
                            style={{
                                transform: `translate(${position.x * 40}px, ${position.y * 40}px)`
                            }}
                            quality={100}
                        />
                        <Image
                            src={'/assets/images/parallax/2.png'}
                            alt={`Picture `}
                            layout="fill"
                            objectFit="contain"
                            className='layer'
                            style={{
                                transform: `translate(${position.x * 40}px, ${position.y * 40}px)`
                            }}
                            quality={100}
                        />
                        <Image
                            src={'/assets/images/parallax/3.png'}
                            alt={`Picture`}
                            layout="fill"
                            objectFit="contain"
                            className='layer img-parallax'
                            style={{
                                transform: `translate(${position.x * 40}px, ${position.y * 40}px)`
                            }}
                            quality={100}
                        />
                        <Image
                            src={'/assets/images/parallax/4.png'}
                            alt={`Picture `}
                            layout="fill"
                            objectFit="contain"
                            className='layer'
                            style={{
                                transform: `translate(${position.x * 40}px, ${position.y * 40}px)`
                            }}
                            quality={100}
                        />
                        <Image
                            src={'/assets/images/parallax/5.png'}
                            alt={`Picture`}
                            layout="fill"
                            objectFit="contain img-parallax"
                            className='layer'
                            style={{
                                transform: `translate(${position.x * 40}px, ${position.y * 40}px)`
                            }}
                            quality={100}
                        />
                        <Image
                            src={'/assets/images/parallax/6.png'}
                            alt={`Picture `}
                            layout="fill"
                            objectFit="contain"
                            className='layer'
                            style={{
                                transform: `translate(${position.x * 40}px, ${position.y * 40}px)`
                            }}
                            quality={100}
                        />
                        <Image
                            src={'/assets/images/parallax/7.png'}
                            alt={`Picture`}
                            layout="fill"
                            objectFit="contain img-parallax"
                            className='layer'
                            style={{
                                transform: `translate(${position.x * 30}px, ${position.y * 60}px)`
                            }}
                            quality={100}
                        />
                        <Image
                            src={'/assets/images/parallax/8.png'}
                            alt={`Picture`}
                            layout="fill"
                            objectFit="contain"
                            className='layer img-parallax'
                            style={{
                                transform: `translate(${position.x * 40}px, ${position.y * 40}px)`
                            }}
                            quality={100}
                        />
                        <Image
                            src={'/assets/images/parallax/9.png'}
                            alt={`Picture `}
                            layout="fill"
                            objectFit="contain img-parallax"
                            className='layer'
                            style={{
                                transform: `translate(${position.x * -40}px, ${position.y * -40}px)`
                            }}
                            quality={100}
                        />
                        <Image
                            src={'/assets/images/parallax/10.png'}
                            alt={`Picture`}
                            layout="fill"
                            objectFit="contain"
                            className='layer'
                            style={{
                                transform: `translate(${position.x * -40}px, ${position.y * -40}px)`
                            }}
                            quality={100}
                        />
                        <Image
                            src={'/assets/images/parallax/11.png'}
                            alt={`Picture`}
                            layout="fill"
                            objectFit="contain img-parallax"
                            className='layer'
                            style={{
                                transform: `translate(${position.x * -40}px, ${position.y * -40}px)`
                            }}
                            quality={100}
                        />
                        <Image
                            src={'/assets/images/parallax/12.png'}
                            alt={`Picture`}
                            layout="fill"
                            objectFit="contain"
                            className='layer'
                            style={{
                                transform: `translate(${position.x * 40}px, ${position.y * 40}px)`
                            }}
                            quality={100}
                        />
                    </>
                </div>
                <div className="absolute inset-x-0 bottom-0 p-8 lg:p-0 flex justify-center">
                    <div className="relative grid place-items-center text-center">
                        <div className="mb-8 grid">
                            <span className="mb-4 lg:text-2xl font-extrabold text-lg uppercase lg:tracking-[.45em] tracking-[.06em] text-white">5 September 2024</span>
                            <button className="btn btn-1 hover-slide-down uppercase">
                                <span>Gabung Sekarang</span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Parallax