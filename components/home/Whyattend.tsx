'use client'

import React, { useEffect } from 'react'
import AOS from 'aos'
import 'aos/dist/aos.css'

const Whyattend = () => {
  useEffect(() => {
    AOS.init({
      duration:2000,
      easing: 'ease-out-cubic',
    })
  },[])
  return (
    <div className="sm:w-full  bg-slate-100 p-2 mx-auto mt-20">
      <div className="max-w-8xl mx-auto lg:my-24 lg:mb-18  lg:px-2 mt-8 px-6">
        <div className="lg:text-xl text-base font-bold text-center text-[#1B1072] mb-10 font-body uppercase" data-aos="fade-down" data-aos-delay="200">
          <h4>why attend</h4>
        </div>
        <div className='lg:px-20 w-full lg:flex items-center justify-between gap-2'>
          <div
            className="relative grid h-80 w-full max-w-[28rem] flex-col overflow-hidden rounded-xl bg-slate-800 bg-clip-border text-gray-700 mt-4" data-aos="zoom-in" data-aos-delay="200">
            <div
              className="absolute inset-0 m-0 h-full w-full overflow-hidden rounded-none bg-transparent bg-cover bg-clip-border bg-center text-gray-700 shadow-none">
            </div>
            <div className="relative p-6 px-6 md:px-12">
              <h2 className="mb-6 block font-sans lg:text-xl text-base font-medium leading-[1.5] tracking-normal text-white font-title uppercase antialiased">
                MENDAPATKAN INSPIRASI DARI PEMBICARA PILIHAN
              </h2>
              <h5 className="block mb-4 font-sans lg:text-base text-sm antialiased leading-snug tracking-normal font-body text-white">
                Marketeers Hangout menghadirkan para pembicara pilihan yang cakap di bidangnya. Mereka akan membagikan inspirasi, kisah sukses, dan wawasan pemasaran yang kreatif. Paparan mereka memberi prespektif baru dan ide kreatif yang bisa diimplementasikan dalam pemasaran.
              </h5>
            </div>
          </div>
          <div
            className="relative grid h-80 w-full max-w-[28rem] flex-col overflow-hidden rounded-xl bg-slate-800 bg-clip-border text-gray-700 mt-4" data-aos="zoom-in" data-aos-delay="200">
            <div
              className="absolute inset-0 m-0 h-full w-full overflow-hidden rounded-none bg-transparent bg-cover bg-clip-border bg-center text-gray-700 shadow-none">
            </div>
            <div className="relative p-6 px-6 md:px-12">
              <h2 className="mb-6 block font-sans lg:text-xl text-base font-medium leading-[1.5] tracking-normal text-white font-title uppercase antialiased">
                BERKUMPUL DENGAN ORANG-ORANG KREATIF
              </h2>
              <h5 className="block mb-4 font-sans lg:text-base text-sm antialiased leading-snug tracking-normal font-body text-white">
                Pemasar tak boleh jadi katak dalam tempurung. Marketeers Hangout menjadi wadah bagi pemasar bertemu dengan orang-orang yang berbeda bidang dan industri. Dengan ini, pemasar akan menemukan ide-ide baru untuk diimplementasikan dalam strategi pemasaran yang kreatif.
              </h5>
            </div>
          </div>
          <div
            className="relative grid h-80 w-full max-w-[28rem] flex-col overflow-hidden rounded-xl bg-slate-800 bg-clip-border text-gray-700 mt-4" data-aos="zoom-in" data-aos-delay="200">
            <div
              className="absolute inset-0 m-0 h-full w-full overflow-hidden rounded-none bg-transparent bg-cover bg-clip-border bg-center text-gray-700 shadow-none">
            </div>
            <div className="relative p-6 px-6 md:px-12">
              <h2 className="mb-6 block font-sans lg:text-xl text-base font-medium leading-[1.5] tracking-normal text-white font-title uppercase antialiased">
                MENIKMATI PENGALAMAN BARU YANG IMERSIF DAN TEATRIKAL
              </h2>
              <h5 className="block mb-4 font-sans lg:text-base text-sm antialiased leading-snug tracking-normal font-body text-white">
                Dengan aksi teatrikal di dalam acara, Marketeers Hangout akan memberikan pengalaman baru dalam berkonferensi secara imersif. Anda akan memperoleh pemahaman dan pengalaman yang lebih dalam tentang storytelling, customer engagement, dan kekuatan experiential marketing.
              </h5>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Whyattend