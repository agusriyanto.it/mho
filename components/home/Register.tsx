import Image from 'next/image'
import Link from 'next/link'
import React from 'react'
import { RiH3 } from 'react-icons/ri'

const Register = () => {
  return (
    <>
      <div className='h-auto p-8 lg:mx-auto lg:max-w-7xl'>
        <div className="lg:text-xl text-base font-bold text-center text-[#1B1072] mb-10 font-body uppercase">
          <h4 className='lg:text-5xl text-black lg:tracking-[.40em] tracking-[.20em] lg:mt-10 font-bold uppercase'>Ticketing</h4>
        </div>
        <div className="text-center lg:mb-10 lg:mt-16 mt-4 uppercase">
          <h4 className='lg:tracking-[.40em] font-bold text-black uppercase'>PLEASE CHOOSE YOUR TICKETS</h4>
          <div className='lg:grid lg:grid-cols-3 items-center justify-center gap-9 mt-6'>
            {/* start card  jogja*/}
            <div className="w-full  mb-5">
              <div className="bg-white rounded-lg m-h-64 p-2 transform hover:translate-y-2 hover:shadow-xl transition duration-300">
                <div className="rounded-lg p-4 bg-black flex flex-col">
                  <div>
                    <h3 className="text-white lg:text-2xl text-base font-bold leading-none">
                      JOGJAKARTA
                    </h3>
                  </div>
                  <div className='mt-12'>
                    <h5 className="text-white lg:text-2xl text-base font-bold leading-none">
                      EARLY BIRD
                    </h5>
                    <span className="text-xs text-gray-200 leading-none">Sampai 30 Juni 2024</span>
                  </div>
                  <div className='mt-10'>
                    <h5 className="text-white lg:text-2xl text-base font-bold leading-none">Rp. 500.000 /Peserta</h5>
                  </div>
                  <div className="grid mt-6">
                    <div className="lg:text-base text-[12px] flex text-white font-light">
                      <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="23" height="23" viewBox="0 0 48 48">
                        <path fill="#c8e6c9" d="M44,24c0,11.045-8.955,20-20,20S4,35.045,4,24S12.955,4,24,4S44,12.955,44,24z"></path><path fill="#4caf50" d="M34.586,14.586l-13.57,13.586l-5.602-5.586l-2.828,2.828l8.434,8.414l16.395-16.414L34.586,14.586z"></path>
                      </svg> &nbsp; Free Seat arrangement
                    </div>
                    <div className="lg:text-base text-[12px] flex text-white font-light">
                      <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="23" height="23" viewBox="0 0 48 48">
                        <path fill="#c8e6c9" d="M44,24c0,11.045-8.955,20-20,20S4,35.045,4,24S12.955,4,24,4S44,12.955,44,24z"></path><path fill="#4caf50" d="M34.586,14.586l-13.57,13.586l-5.602-5.586l-2.828,2.828l8.434,8.414l16.395-16.414L34.586,14.586z"></path>
                      </svg> &nbsp; 2 Coffee Break & Lunch
                    </div>
                    <div className="lg:text-base text-[12px] flex text-white font-light">
                      <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="23" height="23" viewBox="0 0 48 48">
                        <path fill="#c8e6c9" d="M44,24c0,11.045-8.955,20-20,20S4,35.045,4,24S12.955,4,24,4S44,12.955,44,24z"></path><path fill="#4caf50" d="M34.586,14.586l-13.57,13.586l-5.602-5.586l-2.828,2.828l8.434,8.414l16.395-16.414L34.586,14.586z"></path>
                      </svg> &nbsp; e-certificate
                    </div>
                    <div className="lg:text-base text-[12px] flex text-left text-white font-light">
                      <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="25" height="25" viewBox="0 0 48 48">
                        <path fill="#f44336" d="M44,24c0,11.045-8.955,20-20,20S4,35.045,4,24S12.955,4,24,4S44,12.955,44,24z"></path><path fill="#fff" d="M29.656,15.516l2.828,2.828l-14.14,14.14l-2.828-2.828L29.656,15.516z"></path><path fill="#fff" d="M32.484,29.656l-2.828,2.828l-14.14-14.14l2.828-2.828L32.484,29.656z"></path>
                      </svg>&nbsp; 1 Buku Reimagining Operational Excellence
                    </div>
                    <div className="lg:text-base text-[12px] grid grid-col-12 text-left text-white font-light mt-10">
                      <Link href='https://www.loket.com/event/the-12th-indonesia-marketing-festival-joglosemar_IKV' className="col-span-12 px-4 py-2 bg-blue-600 text-green-100 hover:bg-blue-900 duration-300" target='_blank'>Buy Ticket</Link>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            {/* END card */}
            {/* start card  Pekanbaru*/}
            <div className="w-full mb-5">
              <div className="bg-white rounded-lg m-h-64 p-2 transform hover:translate-y-2 hover:shadow-xl transition duration-300">
                <div className="rounded-lg p-4 bg-black flex flex-col">
                  <div>
                    <h3 className="text-white lg:text-2xl text-base font-bold leading-none">
                      Pekanbaru
                    </h3>
                  </div>
                  <div className='mt-12'>
                    <h5 className="text-white lg:text-2xl text-base font-bold leading-none">
                      EARLY BIRD
                    </h5>
                    <span className="text-xs text-gray-200 leading-none">Sampai 30 Juni 2024</span>
                  </div>
                  <div className='mt-10'>
                    <h5 className="text-white lg:text-xl text-base font-bold leading-none">Rp. 500.000 /Peserta</h5>
                  </div>
                  <div className="grid mt-6">
                    <div className="lg:text-base text-[12px] flex text-white font-light">
                      <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="23" height="23" viewBox="0 0 48 48">
                        <path fill="#c8e6c9" d="M44,24c0,11.045-8.955,20-20,20S4,35.045,4,24S12.955,4,24,4S44,12.955,44,24z"></path><path fill="#4caf50" d="M34.586,14.586l-13.57,13.586l-5.602-5.586l-2.828,2.828l8.434,8.414l16.395-16.414L34.586,14.586z"></path>
                      </svg> &nbsp; Free Seat arrangement
                    </div>
                    <div className="lg:text-base text-[12px] flex text-white font-light">
                      <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="23" height="23" viewBox="0 0 48 48">
                        <path fill="#c8e6c9" d="M44,24c0,11.045-8.955,20-20,20S4,35.045,4,24S12.955,4,24,4S44,12.955,44,24z"></path><path fill="#4caf50" d="M34.586,14.586l-13.57,13.586l-5.602-5.586l-2.828,2.828l8.434,8.414l16.395-16.414L34.586,14.586z"></path>
                      </svg> &nbsp; 2 Coffee Break & Lunch
                    </div>
                    <div className="lg:text-base text-[12px] flex text-white font-light">
                      <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="23" height="23" viewBox="0 0 48 48">
                        <path fill="#c8e6c9" d="M44,24c0,11.045-8.955,20-20,20S4,35.045,4,24S12.955,4,24,4S44,12.955,44,24z"></path><path fill="#4caf50" d="M34.586,14.586l-13.57,13.586l-5.602-5.586l-2.828,2.828l8.434,8.414l16.395-16.414L34.586,14.586z"></path>
                      </svg> &nbsp; e-certificate
                    </div>
                    <div className="lg:text-base text-[12px] flex text-left text-white font-light">
                      <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="25" height="25" viewBox="0 0 48 48">
                        <path fill="#f44336" d="M44,24c0,11.045-8.955,20-20,20S4,35.045,4,24S12.955,4,24,4S44,12.955,44,24z"></path><path fill="#fff" d="M29.656,15.516l2.828,2.828l-14.14,14.14l-2.828-2.828L29.656,15.516z"></path><path fill="#fff" d="M32.484,29.656l-2.828,2.828l-14.14-14.14l2.828-2.828L32.484,29.656z"></path>
                      </svg>&nbsp; 1 Buku Reimagining Operational Excellence
                    </div>
                    <div className="lg:text-base text-[12px] grid grid-col-12 text-left text-white font-light mt-10">
                    <Link href='https://www.loket.com/event/the-12th-indonesia-marketing-festival-riau-kepri_IKy' className="col-span-12 px-4 py-2 bg-blue-600 text-green-100 hover:bg-blue-900 duration-300" target='_blank'>Buy Ticket</Link>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            {/* END card */}
            {/* start card  Palembang*/}
            <div className="w-full mb-5">
              <div className="bg-white rounded-lg m-h-64 p-2 transform hover:translate-y-2 hover:shadow-xl transition duration-300">
                <div className="rounded-lg p-4 bg-black flex flex-col">
                  <div>
                    <h3 className="text-white lg:text-xl text-base font-bold leading-none">
                      Palembang
                    </h3>
                  </div>
                  <div className='mt-12'>
                    <h5 className="text-white lg:text-xl text-base font-bold leading-none">
                      EARLY BIRD
                    </h5>
                    <span className="text-xs text-gray-200 leading-none">Sampai 30 Juni 2024</span>
                  </div>
                  <div className='mt-10'>
                    <h5 className="text-white lg:text-xl text-base font-bold leading-none">Rp. 500.000 /Peserta</h5>
                  </div>
                  <div className="grid mt-6">
                    <div className="lg:text-base text-[12px] flex text-white font-light">
                      <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="23" height="23" viewBox="0 0 48 48">
                        <path fill="#c8e6c9" d="M44,24c0,11.045-8.955,20-20,20S4,35.045,4,24S12.955,4,24,4S44,12.955,44,24z"></path><path fill="#4caf50" d="M34.586,14.586l-13.57,13.586l-5.602-5.586l-2.828,2.828l8.434,8.414l16.395-16.414L34.586,14.586z"></path>
                      </svg> &nbsp; Free Seat arrangement
                    </div>
                    <div className="lg:text-base text-[12px] flex text-white font-light">
                      <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="23" height="23" viewBox="0 0 48 48">
                        <path fill="#c8e6c9" d="M44,24c0,11.045-8.955,20-20,20S4,35.045,4,24S12.955,4,24,4S44,12.955,44,24z"></path><path fill="#4caf50" d="M34.586,14.586l-13.57,13.586l-5.602-5.586l-2.828,2.828l8.434,8.414l16.395-16.414L34.586,14.586z"></path>
                      </svg> &nbsp; 2 Coffee Break & Lunch
                    </div>
                    <div className="lg:text-base text-[12px] flex text-white font-light">
                      <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="23" height="23" viewBox="0 0 48 48">
                        <path fill="#c8e6c9" d="M44,24c0,11.045-8.955,20-20,20S4,35.045,4,24S12.955,4,24,4S44,12.955,44,24z"></path><path fill="#4caf50" d="M34.586,14.586l-13.57,13.586l-5.602-5.586l-2.828,2.828l8.434,8.414l16.395-16.414L34.586,14.586z"></path>
                      </svg> &nbsp; e-certificate
                    </div>
                    <div className="lg:text-base text-[12px] flex text-left text-white font-light">
                      <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="25" height="25" viewBox="0 0 48 48">
                        <path fill="#f44336" d="M44,24c0,11.045-8.955,20-20,20S4,35.045,4,24S12.955,4,24,4S44,12.955,44,24z"></path><path fill="#fff" d="M29.656,15.516l2.828,2.828l-14.14,14.14l-2.828-2.828L29.656,15.516z"></path><path fill="#fff" d="M32.484,29.656l-2.828,2.828l-14.14-14.14l2.828-2.828L32.484,29.656z"></path>
                      </svg>&nbsp; 1 Buku Reimagining Operational Excellence
                    </div>
                    <div className="lg:text-base text-[12px] grid grid-col-12 text-left text-white font-light mt-10">
                    <Link href='https://www.loket.com/event/the-12th-indonesia-marketing-festival-sumsel_IKI' className="col-span-12 px-4 py-2 bg-blue-600 text-green-100 hover:bg-blue-900 duration-300" target='_blank'>Buy Ticket</Link>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            {/* END card */}
            {/* start card  Manado*/}
            <div className="w-full  mb-5">
              <div className="bg-white rounded-lg m-h-64 p-2 transform hover:translate-y-2 hover:shadow-xl transition duration-300">
                <div className="rounded-lg p-4 bg-black flex flex-col">
                  <div>
                    <h3 className="text-white lg:text-2xl text-basel font-bold leading-none">
                    Manado
                    </h3>
                  </div>
                  <div className='mt-12'>
                    <h5 className="text-white lg:text-2xl text-base font-bold leading-none">
                      EARLY BIRD
                    </h5>
                    <span className="text-xs text-gray-200 leading-none">Sampai 30 Juni 2024</span>
                  </div>
                  <div className='mt-10'>
                    <h5 className="text-white lg:text-2xl text-base font-bold leading-none">Rp. 500.000 /Peserta</h5>
                  </div>
                  <div className="grid mt-6">
                    <div className="lg:text-base text-[12px] flex text-white font-light">
                      <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="23" height="23" viewBox="0 0 48 48">
                        <path fill="#c8e6c9" d="M44,24c0,11.045-8.955,20-20,20S4,35.045,4,24S12.955,4,24,4S44,12.955,44,24z"></path><path fill="#4caf50" d="M34.586,14.586l-13.57,13.586l-5.602-5.586l-2.828,2.828l8.434,8.414l16.395-16.414L34.586,14.586z"></path>
                      </svg> &nbsp; Free Seat arrangement
                    </div>
                    <div className="lg:text-base text-[12px] flex text-white font-light">
                      <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="23" height="23" viewBox="0 0 48 48">
                        <path fill="#c8e6c9" d="M44,24c0,11.045-8.955,20-20,20S4,35.045,4,24S12.955,4,24,4S44,12.955,44,24z"></path><path fill="#4caf50" d="M34.586,14.586l-13.57,13.586l-5.602-5.586l-2.828,2.828l8.434,8.414l16.395-16.414L34.586,14.586z"></path>
                      </svg> &nbsp; 2 Coffee Break & Lunch
                    </div>
                    <div className="lg:text-base text-[12px] flex text-white font-light">
                      <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="23" height="23" viewBox="0 0 48 48">
                        <path fill="#c8e6c9" d="M44,24c0,11.045-8.955,20-20,20S4,35.045,4,24S12.955,4,24,4S44,12.955,44,24z"></path><path fill="#4caf50" d="M34.586,14.586l-13.57,13.586l-5.602-5.586l-2.828,2.828l8.434,8.414l16.395-16.414L34.586,14.586z"></path>
                      </svg> &nbsp; e-certificate
                    </div>
                    <div className="lg:text-base text-[12px] flex text-left text-white font-light">
                      <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="25" height="25" viewBox="0 0 48 48">
                        <path fill="#f44336" d="M44,24c0,11.045-8.955,20-20,20S4,35.045,4,24S12.955,4,24,4S44,12.955,44,24z"></path><path fill="#fff" d="M29.656,15.516l2.828,2.828l-14.14,14.14l-2.828-2.828L29.656,15.516z"></path><path fill="#fff" d="M32.484,29.656l-2.828,2.828l-14.14-14.14l2.828-2.828L32.484,29.656z"></path>
                      </svg>&nbsp; 1 Buku Reimagining Operational Excellence
                    </div>
                    <div className="lg:text-base text-[12px] grid grid-col-12 text-left text-white font-light mt-10">
                    <Link href='https://www.loket.com/event/the-12th-indonesia-marketing-festival-sulutenggo_IKf' className="col-span-12 px-4 py-2 bg-blue-600 text-green-100 hover:bg-blue-900 duration-300" target='_blank'>Buy Ticket</Link>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            {/* END card */}
            {/* start card  Denpasar*/}
            <div className="w-full mb-5">
              <div className="bg-white rounded-lg m-h-64 p-2 transform hover:translate-y-2 hover:shadow-xl transition duration-300">
                <div className="rounded-lg p-4 bg-black flex flex-col">
                  <div>
                    <h3 className="text-white lg:text-2xl text-base font-bold leading-none">
                    Denpasar
                    </h3>
                  </div>
                  <div className='mt-12'>
                    <h5 className="text-white lg:text-2xl text-base font-bold leading-none">
                      EARLY BIRD
                    </h5>
                    <span className="text-xs text-gray-200 leading-none">Sampai 30 Juni 2024</span>
                  </div>
                  <div className='mt-10'>
                    <h5 className="text-white lg:text-2xl text-base font-bold leading-none">Rp. 500.000 /Peserta</h5>
                  </div>
                  <div className="grid mt-6">
                    <div className="lg:text-base text-[12px] flex text-white font-light">
                      <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="23" height="23" viewBox="0 0 48 48">
                        <path fill="#c8e6c9" d="M44,24c0,11.045-8.955,20-20,20S4,35.045,4,24S12.955,4,24,4S44,12.955,44,24z"></path><path fill="#4caf50" d="M34.586,14.586l-13.57,13.586l-5.602-5.586l-2.828,2.828l8.434,8.414l16.395-16.414L34.586,14.586z"></path>
                      </svg> &nbsp; Free Seat arrangement
                    </div>
                    <div className="lg:text-base text-[12px] flex text-white font-light">
                      <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="23" height="23" viewBox="0 0 48 48">
                        <path fill="#c8e6c9" d="M44,24c0,11.045-8.955,20-20,20S4,35.045,4,24S12.955,4,24,4S44,12.955,44,24z"></path><path fill="#4caf50" d="M34.586,14.586l-13.57,13.586l-5.602-5.586l-2.828,2.828l8.434,8.414l16.395-16.414L34.586,14.586z"></path>
                      </svg> &nbsp; 2 Coffee Break & Lunch
                    </div>
                    <div className="lg:text-base text-[12px] flex text-white font-light">
                      <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="23" height="23" viewBox="0 0 48 48">
                        <path fill="#c8e6c9" d="M44,24c0,11.045-8.955,20-20,20S4,35.045,4,24S12.955,4,24,4S44,12.955,44,24z"></path><path fill="#4caf50" d="M34.586,14.586l-13.57,13.586l-5.602-5.586l-2.828,2.828l8.434,8.414l16.395-16.414L34.586,14.586z"></path>
                      </svg> &nbsp; e-certificate
                    </div>
                    <div className="lg:text-base text-[12px] flex text-left text-white font-light">
                      <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="25" height="25" viewBox="0 0 48 48">
                        <path fill="#f44336" d="M44,24c0,11.045-8.955,20-20,20S4,35.045,4,24S12.955,4,24,4S44,12.955,44,24z"></path><path fill="#fff" d="M29.656,15.516l2.828,2.828l-14.14,14.14l-2.828-2.828L29.656,15.516z"></path><path fill="#fff" d="M32.484,29.656l-2.828,2.828l-14.14-14.14l2.828-2.828L32.484,29.656z"></path>
                      </svg>&nbsp; 1 Buku Reimagining Operational Excellence
                    </div>
                    <div className="lg:text-base text-[12px] grid grid-col-12 text-left text-white font-light mt-10">
                    <Link href='https://www.loket.com/event/the-12th-indonesia-marketing-festival-bali-nusra_IKM' className="col-span-12 px-4 py-2 bg-blue-600 text-green-100 hover:bg-blue-900 duration-300" target='_blank'>Buy Ticket</Link>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            {/* END card */}
            {/* start card  Bandung*/}
            <div className="w-full mb-5">
              <div className="bg-white rounded-lg m-h-64 p-2 transform hover:translate-y-2 hover:shadow-xl transition duration-300">
                <div className="rounded-lg p-4 bg-black flex flex-col">
                  <div>
                    <h3 className="text-white lg:text-2xl text-base font-bold leading-none">
                    Bandung
                    </h3>
                  </div>
                  <div className='mt-12'>
                    <h5 className="text-white lg:text-2xl text-base font-bold leading-none">
                      EARLY BIRD
                    </h5>
                    <span className="text-xs text-gray-200 leading-none">Sampai 30 Juni 2024</span>
                  </div>
                  <div className='mt-10'>
                    <h5 className="text-white lg:text-2xl text-base font-bold leading-none">Rp. 750.000 /Peserta</h5>
                  </div>
                  <div className="grid mt-6">
                    <div className="lg:text-base text-[12px] flex text-white font-light">
                      <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="23" height="23" viewBox="0 0 48 48">
                        <path fill="#c8e6c9" d="M44,24c0,11.045-8.955,20-20,20S4,35.045,4,24S12.955,4,24,4S44,12.955,44,24z"></path><path fill="#4caf50" d="M34.586,14.586l-13.57,13.586l-5.602-5.586l-2.828,2.828l8.434,8.414l16.395-16.414L34.586,14.586z"></path>
                      </svg> &nbsp; Free Seat arrangement
                    </div>
                    <div className="lg:text-base text-[12px] flex text-white font-light">
                      <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="23" height="23" viewBox="0 0 48 48">
                        <path fill="#c8e6c9" d="M44,24c0,11.045-8.955,20-20,20S4,35.045,4,24S12.955,4,24,4S44,12.955,44,24z"></path><path fill="#4caf50" d="M34.586,14.586l-13.57,13.586l-5.602-5.586l-2.828,2.828l8.434,8.414l16.395-16.414L34.586,14.586z"></path>
                      </svg> &nbsp; 2 Coffee Break & Lunch
                    </div>
                    <div className="lg:text-base text-[12px] flex text-white font-light">
                      <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="23" height="23" viewBox="0 0 48 48">
                        <path fill="#c8e6c9" d="M44,24c0,11.045-8.955,20-20,20S4,35.045,4,24S12.955,4,24,4S44,12.955,44,24z"></path><path fill="#4caf50" d="M34.586,14.586l-13.57,13.586l-5.602-5.586l-2.828,2.828l8.434,8.414l16.395-16.414L34.586,14.586z"></path>
                      </svg> &nbsp; e-certificate
                    </div>
                    <div className="lg:text-base text-[12px] flex text-left text-white font-light">
                      <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="25" height="25" viewBox="0 0 48 48">
                        <path fill="#f44336" d="M44,24c0,11.045-8.955,20-20,20S4,35.045,4,24S12.955,4,24,4S44,12.955,44,24z"></path><path fill="#fff" d="M29.656,15.516l2.828,2.828l-14.14,14.14l-2.828-2.828L29.656,15.516z"></path><path fill="#fff" d="M32.484,29.656l-2.828,2.828l-14.14-14.14l2.828-2.828L32.484,29.656z"></path>
                      </svg>&nbsp; 1 Buku Reimagining Operational Excellence
                    </div>
                    <div className="lg:text-base text-[12px] grid grid-col-12 text-left text-white font-light mt-10">
                    <Link href='https://www.loket.com/event/the-12th-indonesia-marketing-festival-jabar-banten_IKZ' className="col-span-12 px-4 py-2 bg-blue-600 text-green-100 hover:bg-blue-900 duration-300" target='_blank'>Buy Ticket</Link>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            {/* END card */}
            {/* start card  Surabaya*/}
            <div className="w-full mb-5">
              <div className="bg-white rounded-lg m-h-64 p-2 transform hover:translate-y-2 hover:shadow-xl transition duration-300">
                <div className="rounded-lg p-4 bg-black flex flex-col">
                  <div>
                    <h3 className="text-white lg:text-2xl text-base font-bold leading-none">
                    Surabaya
                    </h3>
                  </div>
                  <div className='mt-12'>
                    <h5 className="text-white lg:text-2xl text-base font-bold leading-none">
                      EARLY BIRD
                    </h5>
                    <span className="text-xs text-gray-200 leading-none">Sampai 30 Juni 2024</span>
                  </div>
                  <div className='mt-10'>
                    <h5 className="text-white lg:text-2xl text-base font-bold leading-none">Rp. 750.000 /Peserta</h5>
                  </div>
                  <div className="grid mt-6">
                    <div className="lg:text-base text-[12px] flex text-white font-light">
                      <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="23" height="23" viewBox="0 0 48 48">
                        <path fill="#c8e6c9" d="M44,24c0,11.045-8.955,20-20,20S4,35.045,4,24S12.955,4,24,4S44,12.955,44,24z"></path><path fill="#4caf50" d="M34.586,14.586l-13.57,13.586l-5.602-5.586l-2.828,2.828l8.434,8.414l16.395-16.414L34.586,14.586z"></path>
                      </svg> &nbsp; Free Seat arrangement
                    </div>
                    <div className="lg:text-base text-[12px] flex text-white font-light">
                      <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="23" height="23" viewBox="0 0 48 48">
                        <path fill="#c8e6c9" d="M44,24c0,11.045-8.955,20-20,20S4,35.045,4,24S12.955,4,24,4S44,12.955,44,24z"></path><path fill="#4caf50" d="M34.586,14.586l-13.57,13.586l-5.602-5.586l-2.828,2.828l8.434,8.414l16.395-16.414L34.586,14.586z"></path>
                      </svg> &nbsp; 2 Coffee Break & Lunch
                    </div>
                    <div className="lg:text-base text-[12px] flex text-white font-light">
                      <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="23" height="23" viewBox="0 0 48 48">
                        <path fill="#c8e6c9" d="M44,24c0,11.045-8.955,20-20,20S4,35.045,4,24S12.955,4,24,4S44,12.955,44,24z"></path><path fill="#4caf50" d="M34.586,14.586l-13.57,13.586l-5.602-5.586l-2.828,2.828l8.434,8.414l16.395-16.414L34.586,14.586z"></path>
                      </svg> &nbsp; e-certificate
                    </div>
                    <div className="lg:text-base text-[12px] flex text-left text-white font-light">
                      <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="25" height="25" viewBox="0 0 48 48">
                        <path fill="#f44336" d="M44,24c0,11.045-8.955,20-20,20S4,35.045,4,24S12.955,4,24,4S44,12.955,44,24z"></path><path fill="#fff" d="M29.656,15.516l2.828,2.828l-14.14,14.14l-2.828-2.828L29.656,15.516z"></path><path fill="#fff" d="M32.484,29.656l-2.828,2.828l-14.14-14.14l2.828-2.828L32.484,29.656z"></path>
                      </svg>&nbsp; 1 Buku Reimagining Operational Excellence
                    </div>
                    <div className="lg:text-base text-[12px] grid grid-col-12 text-left text-white font-light mt-10">
                    <Link href='https://www.loket.com/event/the-12th-indonesia-marketing-festival-jatim_IKJ' className="col-span-12 px-4 py-2 bg-blue-600 text-green-100 hover:bg-blue-900 duration-300" target='_blank'>Buy Ticket</Link>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            {/* END card */}
            {/* start card  Surabaya*/}
            <div className="w-full mb-5">
              <div className="bg-white rounded-lg m-h-64 p-2 transform hover:translate-y-2 hover:shadow-xl transition duration-300">
                <div className="rounded-lg p-4 bg-black flex flex-col">
                  <div>
                    <h3 className="text-black lg:text-2xl text-base font-bold leading-none">vip </h3>
                  </div>
                  <div className='mt-12'>
                    <h5 className="text-white lg:text-2xl text-base font-bold leading-none">
                    VIP GOLD
                    </h5>
                    <span className="text-xs text-gray-200 leading-none">Most Popula</span>
                  </div>
                  <div className='mt-10'>
                    <h5 className="text-white lg:text-2xl text-base font-bold leading-none">Rp. 1.500.000 /Peserta</h5>
                  </div>
                  <div className="grid mt-6">
                    <div className="lg:text-base text-[12px] flex text-white font-light">
                      <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="23" height="23" viewBox="0 0 48 48">
                        <path fill="#c8e6c9" d="M44,24c0,11.045-8.955,20-20,20S4,35.045,4,24S12.955,4,24,4S44,12.955,44,24z"></path><path fill="#4caf50" d="M34.586,14.586l-13.57,13.586l-5.602-5.586l-2.828,2.828l8.434,8.414l16.395-16.414L34.586,14.586z"></path>
                      </svg> &nbsp; Free Seat arrangement
                    </div>
                    <div className="lg:text-base text-[12px] flex text-white font-light">
                      <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="23" height="23" viewBox="0 0 48 48">
                        <path fill="#c8e6c9" d="M44,24c0,11.045-8.955,20-20,20S4,35.045,4,24S12.955,4,24,4S44,12.955,44,24z"></path><path fill="#4caf50" d="M34.586,14.586l-13.57,13.586l-5.602-5.586l-2.828,2.828l8.434,8.414l16.395-16.414L34.586,14.586z"></path>
                      </svg> &nbsp; 2 Coffee Break & Lunch
                    </div>
                    <div className="lg:text-base text-[12px] flex text-white font-light">
                      <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="23" height="23" viewBox="0 0 48 48">
                        <path fill="#c8e6c9" d="M44,24c0,11.045-8.955,20-20,20S4,35.045,4,24S12.955,4,24,4S44,12.955,44,24z"></path><path fill="#4caf50" d="M34.586,14.586l-13.57,13.586l-5.602-5.586l-2.828,2.828l8.434,8.414l16.395-16.414L34.586,14.586z"></path>
                      </svg> &nbsp; e-certificate
                    </div>
                    <div className="lg:text-base text-[12px] flex text-left text-white font-light">
                    <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="23" height="23" viewBox="0 0 48 48">
                        <path fill="#c8e6c9" d="M44,24c0,11.045-8.955,20-20,20S4,35.045,4,24S12.955,4,24,4S44,12.955,44,24z"></path><path fill="#4caf50" d="M34.586,14.586l-13.57,13.586l-5.602-5.586l-2.828,2.828l8.434,8.414l16.395-16.414L34.586,14.586z"></path>
                      </svg> &nbsp; 1 Buku Reimagining Operational Excellence
                    </div>
                    <div className="lg:text-base text-[12px] grid grid-col-12 text-left text-white font-light mt-10">
                    <Link href='https://payment.markplusinc.com/ticketimf/VIP' className="col-span-12 px-4 py-2 bg-blue-600 text-green-100 hover:bg-blue-900 duration-300" target='_blank'>Buy Ticket</Link>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            {/* END card */}
          </div>
        </div>
      </div>
      {/* <div className="sm:w-full bg-slate-100 p-2 mx-auto">
      <div className="max-w-5xl mx-auto lg:my-24 lg:mb-18  lg:px-2 mt-20 px-6">
        <div className="text-center mb-10 uppercase">
          <h4 className='lg:text-4xl text-base font-bold text-[#1B1072] lg:tracking-[.40em]'>Ticketing</h4>
        </div>
        <div className="grid lg:grid-cols-2 gap-x-4 gap-4">
          <div className="flex flex-col items-center bg-center">
            <div className="w-full h-full bg-blue-900 rounded-3xl">
              <div className="flex flex-col">
                <div className="bg-white relative drop-shadow-sm rounded-3xl p-4 m-4">
                  <div className="flex-none sm:flex">
                    <div className="flex-auto justify-evenly">
                      <div className="flex items-center justify-between">
                        <h2 className="text-2xl font-extrabold uppercase">regular</h2>
                        <div className="flex items-center  my-1">
                          <span className="mr-3 rounded-full bg-white w-full h-auto">
                            <Image
                              src="/../../assets/images/mho_2024logo.png"
                              width={74}
                              height={29}
                              alt="Marketeers Hangout"
                              className="lg:w-full h-auto"
                            />
                          </span>
                        </div>
                      </div>
                      <div className="border-b border-dashed border-b-2 my-5"></div>
                      <div className="flex items-center">
                        <div className="flex flex-col">
                          <div className="w-full flex-none text-6xl text-blue-800 font-bold leading-none">500K</div>
                          <div className="text-xs mt-10">*Randomly selected seat and exclude food and beverage</div>
                        </div>
                      </div>
                      <div className="border-b border-dashed border-b-2 my-5 pt-2">
                        <div className="absolute rounded-full w-5 h-5 bg-blue-900 -mt-2 -left-2"></div>
                        <div className="absolute rounded-full w-5 h-5 bg-blue-900 -mt-2 -right-2"></div>
                      </div>
                      <div className="flex items-center justify-between text-xs">
                        <div>
                          <h6 className="text-slate-500 text-center">MHO-2024</h6>
                        </div>
                        <div>
                          <h6 className="text-slate-500 text-center">5 SEPTEMBER 2024</h6>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="flex flex-col items-center bg-center">
            <div className="w-full h-full bg-blue-900 rounded-3xl">
              <div className="flex flex-col">
                <div className="bg-white relative drop-shadow-sm rounded-3xl p-4 m-4">
                  <div className="flex-none sm:flex">
                    <div className="flex-auto justify-evenly">
                      <div className="flex items-center justify-between">
                        <h2 className="text-2xl font-extrabold uppercase">vip</h2>
                        <div className="flex items-center  my-1">
                          <span className="mr-3 rounded-full bg-white w-full h-auto">
                            <Image
                              src="/../../assets/images/mho_2024logo.png"
                              width={74}
                              height={29}
                              alt="Marketeers Hangout"
                              className="lg:w-full h-auto"
                            />
                          </span>
                        </div>
                      </div>
                      <div className="border-b border-dashed border-b-2 my-5"></div>
                      <div className="flex items-center">
                        <div className="flex flex-col">
                          <div className="w-full flex-none text-6xl text-blue-800 font-bold leading-none">1.500K</div>
                          <div className="text-xs mt-10">*Include reserved seats, food and beverage, access to speaker room</div>
                        </div>
                      </div>
                      <div className="border-b border-dashed border-b-2 my-5 pt-2">
                        <div className="absolute rounded-full w-5 h-5 bg-blue-900 -mt-2 -left-2"></div>
                        <div className="absolute rounded-full w-5 h-5 bg-blue-900 -mt-2 -right-2"></div>
                      </div>
                      <div className="flex items-center justify-between text-xs">
                        <div>
                          <h6 className="text-slate-500 text-center">MHO-2024</h6>
                        </div>
                        <div>
                          <h6 className="text-slate-500 text-center">5 SEPTEMBER 2024</h6>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="text-center lg:mb-32 mb-10 lg:mt-24 mt-16 uppercase">
          <h4 className='lg:tracking-[.40em] uppercase mt-4'>PLEASE CHOOSE YOUR TICKETS</h4>
          <div className='flex flex-wrap items-center justify-center gap-9 mt-8'>
            <div>
              <Link href={'https://www.loket.com/event/marketeers-hangout-2024_I8X'} target='_blank'>
                <Image
                  src="/assets/images/loket.png"
                  width={140}
                  height={80}
                  alt="Marketeers Hangout"
                  className="lg:w-full h-auto"
                />
              </Link>
            </div>
            <div>
              <Link href={'https://www.tiket.com/to-do/marketeers-hangout-2024-marketing-vs-everybody'} target='_blank'>
                <Image
                  src="/assets/images/tiketdotcom.png"
                  width={140}
                  height={80}
                  alt="Marketeers Hangout"
                  className="lg:w-full h-auto"
                />
              </Link>
            </div>
          </div>
        </div>
      </div>
    </div> */}
    </>

  )
}

export default Register