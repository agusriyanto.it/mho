import Image from 'next/image'
import Link from 'next/link'
import React from 'react'
import Slider from "react-slick"

const Speakers = () => {

  return (
    <>
      <div className="w-full mx-auto p-4">
        <div className="w-2/4 mx-auto lg:my-24 lg:mb-18 mt-8">
        <div className="lg:text-xl text-base font-bold text-center text-[#1B1072] mb-10 font-body uppercase">
          <h4 className='lg:text-5xl text-black lg:tracking-[.40em] tracking-[.20em] lg:mt-10 font-bold uppercase'>THE CONNECTORS</h4>
        </div>
          <div className="text-center justify-center gap-4">
              <div className='lg:mt-10 w-full lg:flex items-center'>
                <div>
                  <Image
                    src="/assets/images/imf/hk.png"
                    alt="Marketeers Hangout"
                    width={110}
                    height={112.5}
                    quality={90}
                    className='w-full h-auto'
                    sizes='(max-width: 768px) 100vw, (max-width: 1200px) 50vw, 33vw'
                  />
                  <h2 className='lg:text-2xl text-base text-slate-950 uppercase font-bold'>Hermawan Kartajaya</h2>
                  <span className='lg:text-lg text-sm text-slate-950 font-bold'><i>Founder & Chair  MCorp</i></span>
                </div>
                <div>
                  <Image
                    src="/assets/images/imf/jacky.png"
                    alt="Marketeers Hangout"
                    width={110}
                    height={112.5}
                    quality={90}
                    className='w-full h-auto'
                    sizes='(max-width: 768px) 100vw, (max-width: 1200px) 50vw, 33vw'
                  />
                  <h2 className='lg:text-2xl text-base text-slate-950 uppercase font-bold'>Jacky Mussry</h2>
                  <span className='lg:text-lg text-sm text-slate-950 font-bold'><i>CEO MarkPlus Institute</i></span>
                </div>                
              </div>
          </div>
        </div>
      </div>
    </>

  )
}

export default Speakers