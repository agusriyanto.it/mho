'use client'

import React, { useEffect } from 'react'
import AOS from 'aos'
import 'aos/dist/aos.css'
import { BsCheckCircleFill } from 'react-icons/bs'
import Image from 'next/image'

const Sponsorship = () => {
  useEffect(() => {
    AOS.init({
      duration: 2000,
      easing: 'ease-out-cubic',
    })
  }, [])
  return (
    <div className="sm:w-full mx-auto mt-20">
      <div className="max-w-5xl mx-auto lg:my-24 lg:mb-18  lg:px-2 mt-8 px-6">
        <div className="lg:text-xl text-base font-bold text-center text-[#1B1072] mb-10 font-body uppercase" data-aos="fade-down" data-aos-delay="200">
          <h4>Sponsorship</h4>
        </div>
        <div className="grid lg:grid-cols-2 justify-center gap-4 mb-48">
          <div className="text-left w-full h-auto p-8" data-aos="fade-down" data-aos-delay="200">
            <Image
              src="/../../assets/images/mho_2024logo.png"
              alt="Marketeers Hangout"
              width={200}
              height={112.5}
              quality={90}
              className='w-[100%]'
              sizes='(max-width: 768px) 100vw, (max-width: 1200px) 50vw, 33vw'
            />
          </div>
          <div className="grid font-body font-bold text-black text-base items-center">
            <span className='flex' data-aos="fade-right" data-aos-delay="200"><BsCheckCircleFill />&nbsp; Speaker at keynote/panel session</span>
            <span className='flex mt-2' data-aos="fade-right" data-aos-delay="200"><BsCheckCircleFill /><h2>&nbsp; Media Exposure</h2></span>
            <span className='flex mt-2' data-aos="fade-right" data-aos-delay="200"><BsCheckCircleFill /><h2>&nbsp; On-event logo placement (room backdrop, totem, poster, video bumper)</h2></span>
            <span className='flex mt-2' data-aos="fade-right" data-aos-delay="200"><BsCheckCircleFill /><h2>&nbsp; Priority tickets for clients and partners</h2></span>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Sponsorship