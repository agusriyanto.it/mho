'use client'

import React, { useEffect } from 'react'
import AOS from 'aos'
import 'aos/dist/aos.css'
import { BsCheckCircleFill } from 'react-icons/bs'

const Whyshouldattend = () => {
  useEffect(() => {
    AOS.init({
      duration: 2000,
      easing: 'ease-out-cubic',
    })
  }, [])
  return (
    <div className="sm:w-full  mx-auto mt-20">
      <div className="max-w-4xl mx-auto lg:my-24 lg:mb-18  lg:px-2 mt-8 px-6">
        <div className="lg:text-xl text-base font-bold text-center text-[#1B1072] lg:mb-10 mb-4 font-body uppercase" data-aos="fade-down" data-aos-delay="200">
          <h4 className='lg:text-5xl text-black lg:tracking-[.40em] tracking-[.20em] mt-10 font-bold uppercase'>WHY YOU SHOULD ATTENT</h4>
        </div>
        <div className="grid lg:grid-cols gap-x-4 gap-y-4 lg:mt-16 sm:mb-8 mb-8 text-center justify-center gap-4">
          <div className="grid font-body font-bold text-slate-600 lg:text-xl text-base items-center">
            <span className='flex' data-aos="fade-right" data-aos-delay="200">~&nbsp; Indonesia Marketing Festival merupakan festival akbar dalam bidang Pemasaran  yang diselenggarakan di 7 kota utama di Indonesia dengan konsep yang unik  dan inovatif.
            </span>
            <span className='flex mt-6' data-aos="fade-right" data-aos-delay="200"><h2>~&nbsp; Keberhasilan penyelenggaraan MarkPlus Conference yang telah diselenggarakan  di Jakarta selama 18 tahun dan kemeriahan selama 11 tahun berturut turut  Indonesia Marketing Festival, memberikan optimisme dalam penyelenggaraan  event akbar tahun ini.
            </h2></span>
            <span className='flex mt-6' data-aos="fade-right" data-aos-delay="200"><h2>~&nbsp; Edisi ke-12 Indonesia Marketing Festival 2024 akan mengusung tema LOW BUDGET HIGH IMPACT MARKETING dengan mengangkat  2 segmen menarik yaitu QC (Quality & Cost) Session dan DS (Delivery, Service)
            </h2></span>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Whyshouldattend